package com.inter.exercise;

import com.inter.exercise.entity.Customer;
import com.inter.exercise.entity.EventSite;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

public class TestEventCoordinator {

    @Rule
    public TemporaryFolder tmpDir = new TemporaryFolder();

    @Test
    public void testIsEligible() {
        EventCoordinator planner = new EventCoordinator(new EventSite(0, 0), 0);

        assertFalse(planner.isWithinRange(new Customer(1, "", 90, -180)));
        assertFalse(planner.isWithinRange(new Customer(2, "", -90, 180)));

        // If the customer is at the location already, they are eligible to attend.
        assertTrue(planner.isWithinRange(new Customer(3, "", 0, 0)));
    }

    @Test
    public void testFindAllEligibleCustomers() {
        EventSite site = new EventSite(53.339428, -6.257664);
        EventCoordinator planner = new EventCoordinator(site, 100);

        List<String> serializedCustomers = new ArrayList<>();
        serializedCustomers.add("{\"latitude\": \"53.2451022\", \"user_id\": 4, \"name\": \"Ian Kehoe\", \"longitude\": \"-6.238335\"}");
        serializedCustomers.add("{\"latitude\": \"53.1302756\", \"user_id\": 5, \"name\": \"Nora Dempsey\", \"longitude\": \"-6.2397222\"}");
        serializedCustomers.add("{\"latitude\": \"52.833502\", \"user_id\": 25, \"name\": \"David Behan\", \"longitude\": \"-8.522366\"}");

        Collection<Customer> eligible = planner.findAllEligibleCustomers(serializedCustomers.stream(), Comparator.comparingInt(Customer::getUserId));

        assertEquals(2, eligible.size());
        assertTrue(eligible.stream().noneMatch(cust -> cust.getUserId() == 25));
        assertTrue(eligible.stream().map(Customer::getUserId).allMatch(userId -> (userId == 4 || userId == 5)));
    }

    @Test
    public void testFindAllEligibleCustomersInvalidLocations() {
        EventCoordinator planner = new EventCoordinator(new EventSite(0, 0), 0);
        List<String> serializedCustomers = new ArrayList<>();
        serializedCustomers.add("{\"latitude\": \"54443.2451022\", \"user_id\": 1, \"name\": \"Ian Kehoe\", \"longitude\": \"-623.238335\"}");

        Collection<Customer> eligible = planner.findAllEligibleCustomers(serializedCustomers.stream(), Comparator.comparingInt(Customer::getUserId));
        assertTrue(eligible.isEmpty());
    }

    @Test
    public void testFindAllEligibleCustomersMalformedEntries() {
        EventCoordinator planner = new EventCoordinator(new EventSite(0, 0), 0);
        List<String> serializedCustomers = new ArrayList<>();
        serializedCustomers.add("{\"latitude\": \"name\": \"Ian Kehoe\", \"longitude\": \"-623.238335\"}");
        serializedCustomers.add("{\"latitude\": \"52.833502\", \"user_id\": 25, \"name\": \"David Behan\", \"longitude\": \"-8.522366\"}");

        Collection<Customer> eligible = planner.findAllEligibleCustomers(serializedCustomers.stream(), Comparator.comparingInt(Customer::getUserId));
        assertTrue(eligible.stream().map(Customer::getUserId).allMatch(userId -> userId == 25));
    }

    @Test
    public void testFindAllEligibleCustomersEmptyInput() {
        EventCoordinator planner = new EventCoordinator(new EventSite(0, 0), 100);

        Collection<Customer> eligible = planner.findAllEligibleCustomers(new ArrayList<String>().stream(), Comparator.comparingInt(Customer::getUserId));
        assertTrue(eligible.isEmpty());
    }

    @Test(expected = IOException.class)
    public void testFindAllEligibleCustomersFromNonExistentFile() throws IOException {
        EventCoordinator planner = new EventCoordinator(new EventSite(0, 0), 0);
        planner.findAllEligibleCustomers(Paths.get(tmpDir.getRoot().toString(), "testFindAllEligibleCustomersFromNonExistentFile.testcase"), Comparator.comparingInt(Customer::getUserId));
    }

    @Test(expected = IOException.class)
    public void testFindAllEligibleCustomersFromDirectory() throws IOException {
        EventCoordinator planner = new EventCoordinator(new EventSite(0, 0), 0);
        planner.findAllEligibleCustomers(tmpDir.getRoot().toPath(), Comparator.comparingInt(Customer::getUserId));
    }
}
