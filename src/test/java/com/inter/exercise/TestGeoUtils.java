package com.inter.exercise;

import com.pholser.junit.quickcheck.Mode;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitQuickcheck.class)
public class TestGeoUtils {

    @Property(trials = 100, mode = Mode.EXHAUSTIVE)
    public void testIsValidCoordinate(@InRange(minDouble = -90, maxDouble = 90) double latitude,
                                      @InRange(minDouble = -180, maxDouble = 180) double longitude) {
        assertTrue(GeoUtils.isValidCoordinate(latitude, longitude));
    }

    @Property(trials = 100, mode = Mode.SAMPLING)
    public void testIsValidCoordinateInvalidLatitude(@InRange(minDouble = -360, maxDouble = -90.0001) double latitude,
                                                     @InRange(minDouble = -180, maxDouble = 180) double longitude) {
        assertFalse(GeoUtils.isValidCoordinate(latitude, longitude));
    }

    @Property(trials = 100, mode = Mode.SAMPLING)
    public void testIsValidCoordinateInvalidLongitude(@InRange(minDouble = -90, maxDouble = 90) double latitude,
                                                      @InRange(minDouble = 180.0001, maxDouble = 360) double longitude) {
        assertFalse(GeoUtils.isValidCoordinate(latitude, longitude));
    }

    @Test
    public void testIsValidCoordinateBoundary() {
        assertTrue(GeoUtils.isValidCoordinate(-90, -180));
        assertTrue(GeoUtils.isValidCoordinate(90, 180));
        assertTrue(GeoUtils.isValidCoordinate(-90, 180));
        assertTrue(GeoUtils.isValidCoordinate(90, -180));
    }

    @Test
    public void testDistance() {
        // Distance between two random points.
        double d1 = GeoUtils.distance(-22.969235, 131.529825, 53.313891, -6.604507);
        assertEquals(15153, Math.ceil(d1), 0);

        // Get the distance between one of the pairs of the two most distant points.
        double d2 = GeoUtils.distance(-90, -180, 90, 180);
        assertEquals(20016, Math.ceil(d2), 0);

        // Distance between identical points should be zero.
        double d3 = GeoUtils.distance(-90, -180, -90, -180);
        assertEquals(0, Math.ceil(d3), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDistanceInvalidLatitudeCoordinate() {
        GeoUtils.distance(36_000, -180, -90, 180);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDistanceInvalidLongitudeCoordinate() {
        GeoUtils.distance(90, -180, -90, 18_000);
    }
}
