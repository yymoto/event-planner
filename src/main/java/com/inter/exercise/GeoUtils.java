package com.inter.exercise;

/**
 * Utilities and constants related to geo-location.
 */
public class GeoUtils {
    public static final int EARTH_RADIUS_KM = 6371;

    /**
     * Determines if the given latitude and longitude values are valid.
     *
     * @param latitude A value representing the latitude.
     * @param longitude A value representing the longitude.
     * @return True, if latitude is in the range [-90, 90] and longitude is in the range [-180, 180].
     */
    public static boolean isValidCoordinate(double latitude, double longitude) {
        return latitude >= -90 && latitude <= 90
                && longitude >= -180 && longitude <= 180;
    }

    /**
     * Calculate distances between points which are defined by geographical coordinates in terms of latitude and longitude.
     * The latitudes and longitudes are converted to radians, before calculating the products of the latitudes and longitudes
     * of the two points, respectively.
     * The acos of the central angle between the points (given by the spherical law of cosines) is multiplied by the value
     * of the radius of the Earth to obtain the distance.
     *
     * @param latOne Latitude of the first point - a value between -90 and 90.
     * @param longOne Longitude of the first point - a value between -180 and 180.
     * @param latTwo Latitude of the second point - a value between -90 and 90.
     * @param longTwo Longitude of the second point - a value between -180 and 180.
     * @return The distance between two geo-locations, in kilometers.
     */
    public static double distance(double latOne, double longOne, double latTwo, double longTwo)  {
        if (! isValidCoordinate(latOne, longOne) || ! isValidCoordinate(latTwo, longTwo)) {
            throw new IllegalArgumentException(String.format("Invalid coordinates supplied: (%.6f, %.6f) -> (%.6f,%.6f)", latOne, longOne, latTwo, longTwo));
        }

        final double latOneRadians = Math.toRadians(latOne);
        final double latTwoRadians = Math.toRadians(latTwo);
        final double longOneRadians = Math.toRadians(longOne);
        final double longTwoRadians = Math.toRadians(longTwo);
        final double longDelta = Math.abs(longOneRadians - longTwoRadians);

        final double sinProduct = Math.sin(latOneRadians) * Math.sin(latTwoRadians);
        final double cosProduct = Math.cos(latOneRadians) * Math.cos(latTwoRadians);
        final double distance = Math.acos(sinProduct + (cosProduct * Math.cos(longDelta)));

        return EARTH_RADIUS_KM * distance;
    }
}
