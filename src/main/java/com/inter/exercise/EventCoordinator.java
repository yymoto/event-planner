package com.inter.exercise;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inter.exercise.entity.Customer;
import com.inter.exercise.entity.EventSite;
import com.inter.exercise.entity.GeoLocatableEntity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Encapsulates logic for coordinating customer events.
 */
public class EventCoordinator {
    private static final Logger LOG = Logger.getLogger(EventCoordinator.class.getName());

    // The geo-location of the event.
    private final GeoLocatableEntity eventSite;

    // The radius within which customers are eligible for invitation.
    private final double inclusionRadiusKm;

    public EventCoordinator(EventSite eventSite, double inclusionRadiusKm) {
        this.eventSite = eventSite;
        this.inclusionRadiusKm = inclusionRadiusKm;
    }

    /**
     * Identifies customers eligible for the event from a stream of JSON serialized strings, representing customer metadata.
     * The eligibility criteria is thaft of a customer being located within the inclusion radius.
     * Malformed strings that cannot be deserialized are skipped.
     * Customers whose metadata contains invalid latitude and longitude values are also skipped.
     *
     * @param serializedCustomerStream The stream from which to read strings.
     * @param comparator The order in which to return the collection of eligible customers.
     * @return A sorted collection of customers within the inclusion radius.
     */
    public Collection<Customer> findAllEligibleCustomers(Stream<String> serializedCustomerStream, Comparator<Customer> comparator) {
        final ObjectMapper mapper = new ObjectMapper();

        final PriorityQueue<Customer> filteredCustomers = serializedCustomerStream.map(payload -> deserializeCustomer(payload, mapper))
                .filter(Objects::nonNull)
                .filter(cust -> GeoUtils.isValidCoordinate(cust.getLatitude(), cust.getLongitude()))
                .filter(this::isWithinRange)
                .collect(Collectors.toCollection(() -> new PriorityQueue<>(comparator)));

        final Collection<Customer> eligibleCustomers = new ArrayList<>(filteredCustomers.size());
        while (!filteredCustomers.isEmpty()) {
            eligibleCustomers.add(filteredCustomers.poll());
        }

        return Collections.unmodifiableCollection(eligibleCustomers);
    }

    /**
     * Identifies customers eligible for the event from a file containing a list of serialized customer metadata.
     * The contents of the file is assumed to be a single JSON object per line. which maps to a particular customer.
     * Malformed lines that cannot be deserialized are skipped.
     *
     * @param customerFile The path to the customer metadata file.
     * @param comparator The order in which to return the collection of eligible customers.
     * @return A sorted collection of customers within the inclusion radius.
     * @throws IOException If an error occurred in trying to read the file.
     */
    public Collection<Customer> findAllEligibleCustomers(Path customerFile, Comparator<Customer> comparator) throws IOException {
        if (Files.isDirectory(customerFile)) {
            throw new IOException("Expected a serialized customer file path, but a directory path was provided: " + customerFile.toString());
        } else if (! Files.exists(customerFile)) {
            throw new IOException("No serialized customer file at path " + customerFile.toString());
        }

        try (Stream<String> lineStream = Files.lines(customerFile)) {
            return findAllEligibleCustomers(lineStream, comparator);
        }
    }

    /**
     * Determines if a given location is within the radius of the event.
     *
     * @param locatableEntity An arbitrary geo-location.
     * @return True, if the location is within the radius of the event. False, if outside of it.
     */
    public boolean isWithinRange(GeoLocatableEntity locatableEntity) {
        return eventSite.distanceTo(locatableEntity) <= inclusionRadiusKm;
    }

    /**
     * Deserializes customer metadata and logs any errors that occurred.
     *
     * @param jsonPayload The JSON string to deserialize.
     * @param jsonMapper The JSON deserializer.
     * @return {@link Customer} object, or null if the data could not be deserialized.
     */
    private Customer deserializeCustomer(String jsonPayload, ObjectMapper jsonMapper) {
        try {
            return jsonMapper.readValue(jsonPayload, Customer.class);
        } catch (JsonProcessingException ex) {
            LOG.log(Level.SEVERE, "Skipping malformed customer entry.", ex);

            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Malformed JSON payload: {1}", jsonPayload);
            }

            return null;
        }
    }
}
