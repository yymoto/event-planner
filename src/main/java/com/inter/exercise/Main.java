package com.inter.exercise;

import com.inter.exercise.entity.Customer;
import com.inter.exercise.entity.EventSite;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.out.println("Command format invalid. Expected: java -jar  customer_file_path  radius_km  lat,long");
            System.exit(-1);
        }

        final Path customerFile = Paths.get(args[0]);
        final double inclusionRadius = Double.parseDouble(args[1]);
        final List<Double> eventSiteCoords = Arrays.stream(args[2].split(",")).map(Double::parseDouble).collect(Collectors.toList());

        final EventCoordinator planner = new EventCoordinator(new EventSite(eventSiteCoords.get(0), eventSiteCoords.get(1)), inclusionRadius);
        final Collection<Customer> eligibleCustomers = planner.findAllEligibleCustomers(customerFile, Comparator.comparingInt(Customer::getUserId));

        if (eligibleCustomers.isEmpty()) {
            LOG.log(Level.INFO, "No customers are eligible to attend the event.");
            return;
        }

        final long eligibleCustomerCount = eligibleCustomers.size();

        StringBuilder sb = new StringBuilder();
        eligibleCustomers.forEach(cust -> sb.append(cust.getUserId()).append(" - ").append(cust.getName()).append(", "));

        LOG.log(Level.INFO, "There are {0} customers eligible to attend the event: {1}", new Object[]{ eligibleCustomerCount, sb.toString()});
    }
}
