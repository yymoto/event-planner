package com.inter.exercise.entity;

import com.inter.exercise.GeoUtils;

/**
 * Represents an arbitrary geo-locatable entity.
 */
public interface GeoLocatableEntity {
    double getLatitude();
    double getLongitude();

    /**
     * Calculates the distance between two geo-locatable entities, in kilometers.
     *
     * @param other The geo-locatable entity to calculate the distance to.
     * @return The distance between the two entities, in kilometers.
     */
    default double distanceTo(GeoLocatableEntity other) {
        return GeoUtils.distance(getLatitude(), getLongitude(), other.getLatitude(), other.getLongitude());
    }
}
