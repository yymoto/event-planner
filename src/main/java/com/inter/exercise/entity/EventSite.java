package com.inter.exercise.entity;

/**
 * Represents the geo location of an event.
 */
public class EventSite implements GeoLocatableEntity {
    private final double latitude;
    private final double longitude;

    public EventSite(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }
}
