# Event Planner 

Application for helping to coordinate drinks, dinner and other events.

## Installation
Download the JAR file from [BitBucket Downloads](https://bitbucket.org/yymoto/event-planner/downloads/EventCoordinator-1.0.0-all.jar).

Run the JAR file via the command line, supplying a valid file with a list of JSON metadata, the inclusion radius (in KM) and location of the event.

```bash
java -jar EventCoordinator-1.0.0-all.jar customer.txt 100 53.339428,-6.257664

Nov 19, 2020 10:30:00 A.M. com.inter.exercise.Main main
INFO: There are 16 customers eligible to attend the event: 4 - Ian Kehoe, 5 - Nora Dempsey, 6 - Theresa Enright, 8 - Eoin Ahearn, 11 - Richard Finnegan, 12 - Christina McArdle, 13 - Olive Ahearn, 15 - Michael Ahearn, 17 - Patricia Cahill, 23 - Eoin Gallagher, 24 - Rose Enright, 26 - Stephen McArdle, 29 - Oliver Ahearn, 30 - Nick Enright, 31 - Alan Behan, 39 - Lisa Ahearn,
```

## Requirements
A minimum of JDK 15 is required to run the application.
Older versions of the JDK should be compatible for building and compiling the application from source.

## Build and Test
Gradle is used to build the project.

Assuming a valid JDK exists on the path, to test, run:

```bash
./gradlew check
```

To build an executable JAR, run:

```bash
./gradlew shadowJar
```

Navigate to `$PROJECT_HOME/build/libs/`, to find the built JAR.
